package com.example.demo.rest;


import com.example.demo.dao.UsuarioDAO;
import com.example.demo.model.Usuario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("usuarios")
public class UsuarioRest {

    @Autowired
    private UsuarioDAO usuarioDAO;

    @PostMapping("/create")
    public void create(@RequestBody Usuario usuario){
        usuarioDAO.save(usuario);
    }
    
}